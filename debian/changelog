libauthen-dechpwd-perl (2.007-1~2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 08 Jun 2022 17:36:10 +0100

libauthen-dechpwd-perl (2.007-1~1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Damyan Ivanov ]
  * New upstream version 2.007
  * bump years of upstream copyright
  * Declare conformance with Policy 4.1.1 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Mon, 23 Oct 2017 13:52:54 +0000

libauthen-dechpwd-perl (2.006-3) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/watch: remove obsolete comment.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Declare the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Add explicit build dependency on libmodule-build-perl

 -- Niko Tyni <ntyni@debian.org>  Sat, 06 Jun 2015 20:41:44 +0300

libauthen-dechpwd-perl (2.006-2) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Xavier Guimard ]
  * Update debian/copyright (years and format)
  * Bump Standards-Version to 3.9.4
  * Bump debhelper compatibility to 9 to get hardening flags

 -- Xavier Guimard <x.guimard@free.fr>  Wed, 23 Jan 2013 20:58:08 +0100

libauthen-dechpwd-perl (2.006-1) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * Update my email address.

  [ Maximilian Gass ]
  * New upstream release
    * Add new (build-)dependency libdigest-crc-perl
  * Update copyright years and format
  * Increase debhelper compat level to 8
    * Remove explicit buildsystem in debian/rules, default in v8
  * Bump Standards-Version to 3.9.2: no changes necessary

 -- Maximilian Gass <mxey@cloudconnected.org>  Thu, 23 Jun 2011 13:48:03 +0200

libauthen-dechpwd-perl (2.005-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
  * Use source format 3.0 (quilt).
  * debian/copyright: Formatting changes for current DEP-5 proposal,
    refer to /usr/share/common-licenses/GPL-2, update years of copyright.
  * No longer install README.
  * Use Build.PL.
  * Remove build-dep on perl (>= 5.10) | libmodule-build-perl. Stable
    has perl 5.10.
  * Add build-dep on libtest-pod-coverage-perl, libtest-pod-perl and
    perl (>= 5.10.1) | libparent-perl.
  * Mention module name in description.
  * Bump Standards-Version to 3.9.1 (no changes).
  * Add myself to Uploaders.

  [ Franck Joncourt ]
  * Dropped (build-)dependency on libdata-integer-perl (>= 0.003) since stable
    has 0.003.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 31 Jul 2010 15:05:08 +0900

libauthen-dechpwd-perl (2.004-3) unstable; urgency=low

  * Take over for the Debian Perl Group
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Ivan Kohler <ivan-
    debian@420.am>); Ivan Kohler <ivan-debian@420.am> moved to
    Uploaders.
  * debian/watch: use dist-based URL.
  * dh-make-perl refresh for dh7 etc.
  * Standards-Version 3.8.4

 -- Ivan Kohler <ivan-debian@420.am>  Sat, 24 Apr 2010 20:28:15 -0700

libauthen-dechpwd-perl (2.004-2) unstable; urgency=low

  * Fix FTBFS: Missing Build-Depends on libdata-integer-perl
    (closes: Bug#525578)

 -- Ivan Kohler <ivan-debian@420.am>  Sat, 25 Apr 2009 23:21:08 -0700

libauthen-dechpwd-perl (2.004-1) unstable; urgency=low

  * New upstream version
  * dh-make-perl --refresh
  * Remove patch for FTBFS on sparc; fixed upstream.
  * The "Is this thing on?  I'm just a harmless dependency" release.

 -- Ivan Kohler <ivan-debian@420.am>  Sat, 28 Mar 2009 04:02:06 -0700

libauthen-dechpwd-perl (2.002-4) unstable; urgency=low

  * Fix FTBFS on sparc.  Many thanks to Niko Tyni for diagnosis and Matthew
    Green for the patch.  (closes: Bug#477741)

 -- Ivan Kohler <ivan-debian@420.am>  Fri, 02 May 2008 16:52:26 -0700

libauthen-dechpwd-perl (2.002-3) unstable; urgency=low

  * Update rules file based on current dh-make-perl template for
    Module-Build.xs (closes: Bug#464909).  Yow.

 -- Ivan Kohler <ivan-debian@420.am>  Tue, 12 Feb 2008 10:35:18 -0800

libauthen-dechpwd-perl (2.002-2) unstable; urgency=low

  * Add Build-Dep on libmodule-build-perl used by new upstream version
    (closes: Bug#463881)

 -- Ivan Kohler <ivan-debian@420.am>  Sun, 03 Feb 2008 16:10:32 -0800

libauthen-dechpwd-perl (2.002-1) unstable; urgency=low

  * New upstream release
  * Fix debian/rules for perl 5.10 FTBFS, patch from Damyan Ivanov
    (closes: Bug#463533)

 -- Ivan Kohler <ivan-debian@420.am>  Sat, 02 Feb 2008 10:35:15 -0800

libauthen-dechpwd-perl (2.001-1) unstable; urgency=low

  * Initial Release (closes: Bug#411701).

 -- Ivan Kohler <ivan-debian@420.am>  Tue, 20 Feb 2007 01:05:02 -0800
